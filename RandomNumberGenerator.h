/*
 * RandomNumberGenerator.h
 *
 * Different random number generators for creating random numbers. All 
 * generators use the clcg4 implementation provided in the Materials section of 
 * the Coursework website for creating uniform random numbers along [0..1]
 */

#ifndef RANDOMNUMBERGENERATOR_H
#define RANDOMNUMBERGENERATOR_H

/*
 * Base random number generator class for RNG abstraction (if desired).
 */
class RandomNumberGenerator
{
public:
   RandomNumberGenerator();
   virtual ~RandomNumberGenerator();

   // Define a function for all RNGs that allows the instantiator to force a 
   // a new seed for the RNG.
   void Reseed();
   unsigned int GetUnderlyingRng() const;

   // Abstract function that inherited RNGs will override with their 
   // implementation for generating random variables based on their 
   // distribution.
   virtual double GenerateRandomNumber() = 0;

protected:
   // Random number generator this object uses for generating uniform RNGs
   unsigned int rng;
};

/*
 * Triangle RNG. Generates a random number on the interval [a..b] with mode 
 * of m where the pdf from [a..m] and [m..b] is linear.
 */
class TriangleRandomNumberGenerator : public RandomNumberGenerator
{
public:
   TriangleRandomNumberGenerator(double lowerBound, double upperBound,
      double peakValue);
   virtual ~TriangleRandomNumberGenerator();

   double GenerateRandomNumber();

private:
   double a, b, scaledM;
};

/*
 * Exponential RNG. Generates an exponential random number with mean beta on 
 * the interval [0..Inf]
 */
class ExponentialRandomNumberGenerator : public RandomNumberGenerator
{
public:
   ExponentialRandomNumberGenerator(double meanVal);
   virtual ~ExponentialRandomNumberGenerator();

   double GenerateRandomNumber();

   static double GenerateExponentialRandomNumber(unsigned int rng,
      double meanVal);

private:
   double beta;
};

class TruncatedExponentialRandomNumberGenerator : public RandomNumberGenerator
{
public:
   TruncatedExponentialRandomNumberGenerator(double meanVal, double min, double max);
   virtual ~TruncatedExponentialRandomNumberGenerator();

   double GenerateRandomNumber();

private:
   double beta, min, max;
};

/*
 * Bernoulli RNG. Generates a discrete RV on the range [0,1] where the 
 * probability of a 1 is p.
 */
class BernoulliRandomNumberGenerator : public RandomNumberGenerator
{
public:
   BernoulliRandomNumberGenerator(double probabilityOfSuccess);
   virtual ~BernoulliRandomNumberGenerator();

   double GenerateRandomNumber();

private:
   double p;
};

/*
 * Uniform RNG (works mostly as an object interface for the clcg4 RNG).
 * Generates a uniform random number on the interval [0..1]
 */
class UniformRandomNumberGenerator: public RandomNumberGenerator {
public:
   UniformRandomNumberGenerator(double min, double max);
   virtual ~UniformRandomNumberGenerator();

   double GenerateRandomNumber();

private:
   double min;
   double max;
};

/*
 * Normal RNG.  Generates a Normal RV with standard deviation
 * and mean provided.
 */
class NormalRandomNumberGenerator: public RandomNumberGenerator {
public:
   NormalRandomNumberGenerator(double standardDeviation, double mean);
   virtual ~NormalRandomNumberGenerator();

   double GenerateRandomNumber();

private:
   double mean;
   double standardDeviation;
};

class TruncatedNormalRandomNumberGenerator: public RandomNumberGenerator {
public:
   TruncatedNormalRandomNumberGenerator(double standardDeviation, double mean,
         double min, double max);
   virtual ~TruncatedNormalRandomNumberGenerator();

   double GenerateRandomNumber();

private:
   double mean;
   double standardDeviation;
   double min;
   double max;
};
#endif // RANDOMNUMBERGENERATOR_H
