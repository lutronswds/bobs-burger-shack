// BobsBurgerShack.cpp : Defines the entry point for the console application.
//

#include "RandomNumberGenerator.h"
#include <limits>
#include <algorithm>
#include <iostream>

using namespace std;

/*
 * Math Stuff
 */
#define Z99             2.576
#define Z95             1.960
#define NUM_TEST_TRIALS 100

/*
 * Magic Numbers
 */
#define EVENT_CLOCK_DISABLED  -1.0
#define SERVER_IDLE           EVENT_CLOCK_DISABLED

/*
 * Constants for configuring our code to implement Bob's assumptions
 */
#define AVERAGE_CUSTOMER_INTERARRIVAL_TIME         (30)
#define AVERAGE_SERVICE_TIME                       (60)
#define MIN_SERVICE_TIME                           (30)
#define MAX_SERVICE_TIME                           (240)
#define LUNCH_TIME_LENGTH                          (10800)
/*
 * Constants that Bob has control over.  We'll change this and recompile to see
 * how this affects the output.
 */
#define NUM_SERVERS                                (4)
#define EPSILON                                    0.01

/*
 * These objects generate random numbers according to the statistical
 * distributions we've specified in our assumptions.  One of the challenges
 * of constructing a simulation is deciding which distributions to use.  For
 * example, we might know that a customer arrives on average every 30 seconds,
 * but it doesn't make sense for us to write code that generates a customer
 * exactly every 30 seconds.  We know in reality that customer flow will
 * sometimes be a trickle and sometimes be a wave.  We choose an exponential
 * distribution to show this for reasons we'll discuss in the advanced section.
 * (This actually a well-understood random process known as a Poisson process
 * that is commonly used to model queuing problems)
 */
static ExponentialRandomNumberGenerator* MagicCustomerGenerator; 
static TruncatedExponentialRandomNumberGenerator* MenialLaborGenerator; 

/*
 * Run a single instance of lunch time.
 * This routine will use the statistical distributions we've specified to run a
 * lunchtime scenario.  Each execution of this function will be different than
 * the last.  We're trying to stress our system with different conditions to get
 * an idea of how it will respond.  Rather than specify each of these
 * conditions, we're relying on the diversity of a large number of randomly
 * generated scenarios to give us a picture of how the system works overall.
 * We'll return some statistic of our choice here that we'll try use to make
 * a business decision for Bob.
 */
static double letsDoLunch(void);
static double letsDoLunchSolution(void);

/*
 * main - where the magic happens!
 */
int main(int argc, char* argv[])
{
   ExponentialRandomNumberGenerator magicCustomerGenerator(AVERAGE_CUSTOMER_INTERARRIVAL_TIME);
   TruncatedExponentialRandomNumberGenerator menialLaborGenerator(AVERAGE_SERVICE_TIME, MIN_SERVICE_TIME, MAX_SERVICE_TIME);
   MagicCustomerGenerator = &magicCustomerGenerator;
   MenialLaborGenerator = &menialLaborGenerator;
   /*
    * Figure out how many trials we need with the math below.  We discussed
    * this in our first symposium on simulation.  We're going to run a number
    * of iterations on our scenario, figure out some basic statistics, and
    * calculate the number of trials we need to meet our desired confidence
    * interval.  Note that this code uses the numerically stable algorithms
    * for calculating sample variance and mean.
    */
   
   double sum = 0;
   double v = 0;
   cout << "Running " << NUM_TEST_TRIALS << " test trials to determine variance and mean." << endl;
   for (int trialIndex = 1; trialIndex <= NUM_TEST_TRIALS; trialIndex++)
   {
      
      //
      double statistic = letsDoLunch();
      
      if (trialIndex != 1)
      {
         double temp = sum - (double) ((trialIndex - 1) * statistic);
         
         sum += (double) statistic;
         
         v += (temp / (double) (trialIndex - 1)) * (temp / (double) trialIndex);
      }
      else
      {
         v = 0.0;
         sum = (double) statistic;
      }
   }
   double sampleAverage = sum / NUM_TEST_TRIALS;
   double sampleVariance = v / (double) (NUM_TEST_TRIALS - 1);
   
   // The reason for burning the cycles above. . .how many iterations do we
   // actually need?
   int numTrialsRequired = (int) ((sampleVariance * Z99 * Z99)
      / (EPSILON * EPSILON * sampleAverage * sampleAverage));
   cout << numTrialsRequired << " trials needed for statistical confidence." << endl;
   
   /*
    * Let's do this one more time now that we've got the number of trials we
    * need.
    */
   sum = 0;
   v = 0;
   for (int trialIndex = 1; trialIndex <= numTrialsRequired; trialIndex++)
   {
      
      double statistic = letsDoLunch();
      
      if (trialIndex != 1)
      {
         double temp = sum - (double) ((trialIndex - 1) * statistic);
         
         sum += (double) statistic;
         
         v += (temp / (double) (trialIndex - 1)) * (temp / (double) trialIndex);
      }
      else
      {
         v = 0.0;
         sum = (double) statistic;
      }
   }
   double expectedValue = sum / (double) numTrialsRequired;
   
   cout << "The expected value of your selected statistic is within " << EPSILON
      << " percent of " << expectedValue << " with 99% confidence" << endl;
   
   // while (1)
      // ; // stop here so VS users can see the output on the prompt.
      
   return 0;
}

static double letsDoLunch(void){
	return letsDoLunchSolution();
	// Skeleton for implementation by badasses

    // Define state variables
    // Define event clocks
    // Define time

    // Set initial conditions (queue = 0, time = 0)
    // Initialize invalid event clocks (servers)
    // Generate first event (customer arrival)

    // while (time < lunch time length)

       // search event clocks for next event to occur (skip those with invalid times (-1))
       // store time to next event

       // increment time by time to next event
       // decrement valid event clocks by time to next event

       // search event clocks for time remaining = 0
          // process events for clocks with time remaining = 0

    // We'll discuss instrumentation in the advanced exercise!
}

#define CUSTOMER_ARRIVAL_EVENT_CLOCK            0
#define SERVER_ORDER_READY_EVENT_CLOCK          1

// Simulate lunchtime
static double letsDoLunchSolution(void)
{
   
   // The fire in which we burn. . .
   double time = 0;
   /*
    * Time remaining until each event occurs
    * 1 reserved for the next customer arrival
    * Others variable based on the number of servers we compile in
    */
   double eventClocks[1 + NUM_SERVERS];
   // State variable - the number of customers in the queue
   unsigned int queuedCustomers = 0;
   
   // Statistics
   unsigned int maxQueuedCustomers = 0;
   unsigned int customersServed = 0;
   double totalWaitedTime = 0;
   
   /*
    * We need to set initial conditions for our system.  We set up our state
    * variable for the customer queue above.  Now we need to set up our event
    * clocks.  The event clocks always hold the amount of time remaining until
    * that event occurs.  An event clock that holds a negative value (-1) is
    * disabled and doesn't figure in to our next state transition.  In more
    * advanced simulations, events can be cancelled by state transitions.  This
    * is done by setting the event clock to -1 before the time expires.
    *
    * The customer arrival event clock holds the amount of time remaining until
    * the next customer walks through the door.  The server event clocks
    * contain the amount of time remaining until the server finishes the order
    * they are working on.  Since the queue holds no customers, the servers
    * start out idle in our system so their event clocks are disabled.  We then
    * generate the first customer arrival time as shown below.
    */
   eventClocks[CUSTOMER_ARRIVAL_EVENT_CLOCK] =
      MagicCustomerGenerator->GenerateRandomNumber();
   for (int i = 0; i < NUM_SERVERS; i++)
   {
      eventClocks[1 + i] = EVENT_CLOCK_DISABLED;
   }
   
   /*
    * This is our main state machine.  We're going to run until lunch time is
    * over - so three hours of virtual time.  Some simultions run until a
    * certain condition occurs and then look at time itself as the statistic.
    * In our case, we're just running for a certain amount of time and looking
    * at the behavior of the system.
    */
   while (time < LUNCH_TIME_LENGTH)
   {
      /*
       * We're going to search the event clocks array for the lowest time now.
       * That's going to give us the event that's going to happen next.  We'll
       * initialize our time to next event to the numeric limits of the double
       * type.  We can only go down from there. . .
       */
      double timeToNextEvent = numeric_limits<double>::max();
      for (unsigned int i = 0; i < sizeof(eventClocks) / sizeof(double); i++)
      {
         if (eventClocks[i] != EVENT_CLOCK_DISABLED)
         {
            timeToNextEvent = min(timeToNextEvent, eventClocks[i]);
         }
      }
      
      /*
       * How that we know how long it will take to get to the next event, let's
       * decrement all of the enabled event clocks by that amount of time.
       * We'll process the event a little later by figuring out which event
       * clock evaluates to 0.
       */
      for (unsigned int i = 0; i < sizeof(eventClocks) / sizeof(double); i++)
      {
         if (eventClocks[i] != EVENT_CLOCK_DISABLED)
         {
            eventClocks[i] -= timeToNextEvent;
         }
      }
      
      // Advance time.
      time += timeToNextEvent;
      /*
       * This is a statistic that we're tracking.  We'll have checks and
       * operations like this all over the code in a real simulation.  This is
       * how we instrument things we care about.
       */
      totalWaitedTime += queuedCustomers * timeToNextEvent;
      
      /*
       * This is where we figure out which event actually happened and process
       * it.  There is an obvious concern about the possibility that two events
       * could happen at the same time.  This is a tremendously low probability
       * event and we accept the implicit priority of events as they're handled
       * below in that case.
       */
      
      // Customer Arrived
      if (eventClocks[CUSTOMER_ARRIVAL_EVENT_CLOCK] == 0)
      {
         // a customer arrived, increment the queue
         queuedCustomers++;
         // generate the time until the next customer arrives
         eventClocks[CUSTOMER_ARRIVAL_EVENT_CLOCK] =
            MagicCustomerGenerator->GenerateRandomNumber();
         
         // if there is a free server?
         for (unsigned int i = SERVER_ORDER_READY_EVENT_CLOCK;
            i < sizeof(eventClocks) / sizeof(double); i++)
         {
            if (eventClocks[i] == EVENT_CLOCK_DISABLED)
            {
               // decrement the queue
               queuedCustomers--;
               // get them started on the order
               eventClocks[i] = MenialLaborGenerator->GenerateRandomNumber();
               break;
            }
         }
         // Track the max queue size.
         maxQueuedCustomers = max(queuedCustomers, maxQueuedCustomers);
      }
      // Server finished an order
      for (unsigned int i = SERVER_ORDER_READY_EVENT_CLOCK;
         i < sizeof(eventClocks) / sizeof(double); i++)
      {
         if (eventClocks[i] == 0)
         {
            customersServed++;
            // if there is anyone in the queue
            if (queuedCustomers > 0)
            {
               // decrement the queue
               queuedCustomers--;
               // get them started on the order
               eventClocks[i] = MenialLaborGenerator->GenerateRandomNumber();
            }
            else
            {
               // server is idle until next person shows up
               eventClocks[i] = EVENT_CLOCK_DISABLED;
            }
         }
      }
   }
   
   /*
    * Return out statistic of choice. . .
    */
   //return totalWaitedTime/(double)customersServed;
   //return totalWaitedTime;
   //return (double)customersServed;
   return (double) maxQueuedCustomers;
}

