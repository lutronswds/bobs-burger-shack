CXXFLAGS = -g -Wall -fmessage-length=0

OBJS =		BobsBurgerShack.o clcg4.o RandomNumberGenerator.o

LIBS =

TARGET =	BobsBurgerShack.exe

$(TARGET):	$(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LIBS)

all:	$(TARGET)

clean:
	rm -f $(OBJS) $(TARGET)
