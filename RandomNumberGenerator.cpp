/*
 * RandomNumberGenerator.cpp
 *
 * Implementation file for random number generators used in simulations.
 */

#include <cmath>
#include <vector>
#include <cassert>
#include "clcg4.h"
#include "RandomNumberGenerator.h"

/*
 * Generator Allocator. The generator allocator will be a static object that 
 * prevents multiple RNGs from using the same base generator. It will also 
 * take care of initializing the underlying clcg4 generator used by all the 
 * generators.
 */
class GeneratorAllocator
{
public:
   GeneratorAllocator(unsigned int numGenerators);
   virtual ~GeneratorAllocator();

   // Used by an RNG to get a uniform RNG. Number returned should be checked 
   // against the maximum generator value since this will be the return value 
   // if all available generators are in use.
   unsigned int GetAvailableGenerator();
   unsigned int GetMaxGenValue() const;

   // Used by RNG destructor so that others may use it's old generator.
   void ReturnGenerator(unsigned int rng);

private:
   std::vector<unsigned int> availableGenerators;
   unsigned int numGenerators;
};

GeneratorAllocator::GeneratorAllocator(unsigned int numGenerators) :
   numGenerators(numGenerators)
{
   for (unsigned int i = 0; i < numGenerators; i++)
   {
      this->availableGenerators.push_back(i);
   }
   InitDefault();
}

GeneratorAllocator::~GeneratorAllocator()
{
}

unsigned int GeneratorAllocator::GetAvailableGenerator()
{
   if (this->availableGenerators.empty())
   {
      return this->numGenerators;
   }
   else
   {
      unsigned int retVal =
         this->availableGenerators[this->availableGenerators.size() - 1];
      this->availableGenerators.pop_back();
      return retVal;
   }
}

unsigned int GeneratorAllocator::GetMaxGenValue() const
{
   return this->numGenerators;
}

void GeneratorAllocator::ReturnGenerator(unsigned int rng)
{
   // Reset to the original seed as if it was never checked out.
   InitGenerator(rng, InitialSeed);
   
   // Return it to the queue, hope someone isn't returning one that wasn't 
   // checked out.
   this->availableGenerators.push_back(rng);
}

static GeneratorAllocator TheAllocator(Maxgen);

/*
 * Base RNG Class
 */

RandomNumberGenerator::RandomNumberGenerator()
{
   unsigned int generator = TheAllocator.GetAvailableGenerator();
   
   // Check if we ran out generators...
   assert (generator != TheAllocator.GetMaxGenValue());
   
   this->rng = generator;
}

RandomNumberGenerator::~RandomNumberGenerator()
{
   TheAllocator.ReturnGenerator(this->rng);
}

void RandomNumberGenerator::Reseed()
{
   InitGenerator(this->rng, NewSeed);
}

unsigned int RandomNumberGenerator::GetUnderlyingRng() const
{
   return (this->rng);
}

/*
 * Triangle RNG
 */

TriangleRandomNumberGenerator::TriangleRandomNumberGenerator(double lowerBound,
   double upperBound, double peakValue) :
   RandomNumberGenerator(), a(lowerBound), b(upperBound)
{
   // We store a scaled value of M that resides in the interval [0..1], i.e. 
   // the interval of our uniform RNG. Then we scale it back before we return 
   // the generated RV. This avoids possible headaches such as taking the 
   // sqrt of a negative number.
   assert(this->a < this->b);
   assert((peakValue >= a) && (peakValue <= b));
   
   this->scaledM = (peakValue - this->a) / (this->b - this->a);
}

TriangleRandomNumberGenerator::~TriangleRandomNumberGenerator()
{
}

double TriangleRandomNumberGenerator::GenerateRandomNumber()
{
   // See Law [2007] Section 8.3.15. Generation of triangle RVs using the 
   // inversion method.
   double U = GenVal(this->rng);
   double X;
   
   if (U <= this->scaledM)
   {
      X = sqrt(this->scaledM * U);
   }
   else
   {
      X = 1 - sqrt((1 - this->scaledM) * (1 - U));
   }
   
   // Scale back up to our desired range.
   return (this->a + (this->b - this->a) * X);
}

/*
 * Exponential RNG
 */

ExponentialRandomNumberGenerator::ExponentialRandomNumberGenerator(
   double meanVal) :
   RandomNumberGenerator(), beta(meanVal)
{
   assert(beta > 0);
}

ExponentialRandomNumberGenerator::~ExponentialRandomNumberGenerator()
{
}

double ExponentialRandomNumberGenerator::GenerateRandomNumber()
{
   return -(this->beta) * log(GenVal(this->rng));
}

double ExponentialRandomNumberGenerator::GenerateExponentialRandomNumber(
   unsigned int RNG, double meanVal)
{
   if ((RNG < Maxgen) && (meanVal > 0))
   {
      return -meanVal * log(GenVal(RNG));
   }
   
   // Invalid calling parameters
   return 0;
}

TruncatedExponentialRandomNumberGenerator::TruncatedExponentialRandomNumberGenerator(
   double meanVal, double min, double max) :
   RandomNumberGenerator(), beta(meanVal), min(min), max(max)
{

}

TruncatedExponentialRandomNumberGenerator::~TruncatedExponentialRandomNumberGenerator()
{
}

double TruncatedExponentialRandomNumberGenerator::GenerateRandomNumber()
{
   double value = 0;
   do{
      value = -(this->beta) * log(GenVal(this->rng));
   } while ((value < min) || (value > max));

   return value;
}
/*
 * Bernoulli RNG
 */

BernoulliRandomNumberGenerator::BernoulliRandomNumberGenerator(
   double probabilityOfSuccess) :
   RandomNumberGenerator()
{
   //assert((probabilityOfSuccess < 1) && (probabilityOfSuccess > 0));
   
   this->p = probabilityOfSuccess;
}

BernoulliRandomNumberGenerator::~BernoulliRandomNumberGenerator()
{
}

double BernoulliRandomNumberGenerator::GenerateRandomNumber()
{
   // See Law [2007] Section 8.4.1. Generation of Bernoulli using the 
   // inversion (a.k.a. naive) method.
   if (GenVal(this->rng) <= this->p)
   {
      return 1;
   }
   
   return 0;
}

/*
 * Uniform RNG
 */

UniformRandomNumberGenerator::UniformRandomNumberGenerator(double min, double max) :
   RandomNumberGenerator() {
	this->min = min;
	this->max = max;
}

UniformRandomNumberGenerator::~UniformRandomNumberGenerator() {
}

double UniformRandomNumberGenerator::GenerateRandomNumber() {
   return (GenVal(this->rng))*(this->max - this->min) + this->min;
}

/*
 * Normal RNG
 */

NormalRandomNumberGenerator::NormalRandomNumberGenerator(double standardDeviation, double mean) :
   RandomNumberGenerator() {
	this->standardDeviation = standardDeviation;
	this->mean = mean;
}

NormalRandomNumberGenerator::~NormalRandomNumberGenerator() {
}

double NormalRandomNumberGenerator::GenerateRandomNumber() {
	double x1, x2, w, y;

	do {
		x1 = 2.0 * GenVal(this->rng) - 1.0;
		x2 = 2.0 * GenVal(this->rng) - 1.0;
		w = x1 * x1 + x2 * x2;
	} while ( w >= 1.0 );

	w = sqrt( (-2.0 * log( w ) ) / w );
	y = x1 * w;

	y *= this->standardDeviation;
	y += this->mean;

	return y;
}
TruncatedNormalRandomNumberGenerator::TruncatedNormalRandomNumberGenerator(double standardDeviation, double mean,
      double min, double max) : RandomNumberGenerator() {
    this->standardDeviation = standardDeviation;
    this->mean = mean;
    this->min = min;
    this->max = max;
}

TruncatedNormalRandomNumberGenerator::~TruncatedNormalRandomNumberGenerator() {
}

double TruncatedNormalRandomNumberGenerator::GenerateRandomNumber() {
    double x1, x2, w, y;

    generate:
    do {
        x1 = 2.0 * GenVal(this->rng) - 1.0;
        x2 = 2.0 * GenVal(this->rng) - 1.0;
        w = x1 * x1 + x2 * x2;
    } while ( w >= 1.0 );

    w = sqrt( (-2.0 * log( w ) ) / w );
    y = x1 * w;

    y *= this->standardDeviation;
    y += this->mean;

    if ((y < min) || (y > max)){
       goto generate;
    }

    return y;
}
